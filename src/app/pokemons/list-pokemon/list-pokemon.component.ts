import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Pokemon} from '../pokemon';
import { PokemonsService } from '../pokemons.service';

@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrls: ['./list-pokemon.component.css'],
})

export class ListPokemonComponent implements OnInit {
  title = 'Pokémons';
  name = "Angular";
  pokemons: Pokemon[] = null;

  constructor(private router: Router, private pokemonsService: PokemonsService ){}
  ngOnInit() {
    this.pokemons = this.pokemonsService.getPokemons();
  }

  selectPokemon(pokemon: Pokemon) {
    const link = ['/pokemon', pokemon.id];
    this.router.navigate(link);
    console.log('Vous avez sélectionné ' + pokemon.name);
  }
}
