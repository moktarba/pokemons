import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailPokemonComponent } from './detail-pokemon/detail-pokemon.component';
import { ListPokemonComponent } from './list-pokemon/list-pokemon.component';
import { PokemonTypeColorPipe } from './pokemon-type-color.pipe';
import { BorderCardDirective } from './border-card.directive';
import { AnimateCardDirective } from './border-animate.directive';
import { PokemonRoutingModule } from './pokemons-routing.module';
import { PokemonsService } from './pokemons.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BorderCardDirective,
    AnimateCardDirective,
    PokemonTypeColorPipe,
    ListPokemonComponent,
    DetailPokemonComponent
  ],
  providers : [PokemonsService]
})
export class PokemonsModule { }
