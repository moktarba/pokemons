import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {Pokemon} from './pokemons/pokemon';
import {POKEMONS} from './pokemons/mock-pokemons';

@Component({
  selector: 'pokemon-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent   {
  title = "Mes Pokemons"
}
