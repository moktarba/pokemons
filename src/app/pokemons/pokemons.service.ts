import { Injectable } from '@angular/core';
import { Pokemon } from './pokemon';
import { POKEMONS } from './mock-pokemons';

@Injectable({
  providedIn: 'root'
})
export class PokemonsService {


  constructor() { }

  getPokemons(): Pokemon[] {
    return POKEMONS;
  }

  getPokemon( id: number ) {
    const pokemons: Pokemon[] = this.getPokemons();
    for (let index = 0; index < pokemons.length; index ++) {
      if ( id === pokemons[index].id ) {
        return pokemons[index];
      }
    }
  }
}
