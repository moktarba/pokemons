import {Directive, ElementRef, Input, HostListener } from "@angular/core";

@Directive({
  selector: '[pkmnAnimate]',
})
export class AnimateCardDirective{
  private initialeRotation : string = '0';
  private defaultRotation : string = '360';

  constructor(private el : ElementRef){
    this.setAnimate(this.initialeRotation);
  }
  @Input('pkmnAnimate') animateChoice : string;

  @HostListener('mouseenter') onMouseEnter(){
    this.setAnimate(this.animateChoice || this.defaultRotation);
    console.log(this.animateChoice);
  }

  @HostListener('mouseleave') onMouseLeave(){
    this.setAnimate(this.initialeRotation);
  }

  private setAnimate(rotation : string){
    this.el.nativeElement.style.transform = 'rotate('+rotation+'deg)';
    // this.el.nativeElement.width = rotation+'px';
  }
}
