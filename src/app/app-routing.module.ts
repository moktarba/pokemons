import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found.component';
import { ListPokemonComponent } from './pokemons/list-pokemon/list-pokemon.component';
import { DetailPokemonComponent } from './pokemons/detail-pokemon/detail-pokemon.component';

const appRoutes : Routes = [
  { path : 'pokemons', component : ListPokemonComponent },
  { path : 'pokemon/:id', component : DetailPokemonComponent },
  { path : '', redirectTo : 'pokemons', pathMatch : 'full' },
  { path : '**', component : PageNotFoundComponent }
];

@NgModule({
  imports : [
    RouterModule.forRoot(appRoutes, {enableTracing : true})
  ],
  exports : [RouterModule]
})
export class AppRoutingModule{}
